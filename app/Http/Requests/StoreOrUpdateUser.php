<?php

namespace App\Http\Requests;

//use Hash;
use App\Models\User;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class StoreOrUpdateUser extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        /* @var User $user */
        $user = $this->route('user');
        $userId = is_null($user) ? 0 : $user->getAttribute('id');

        $rules = [
            'first_name' => [
                'between:1,50',
                'required'
            ],
            'last_name' => [
                'between:1,50',
                'required'
            ],
            'email' => [
                'email',
                'max:100',
                'unique:users,email' . (0 === $userId ? '' : ",$userId"),
                'required',
            ],
            'password' => [
                'string',
                'min:8',              // must be at least 8 characters in length
                'regex:/[a-z]/',      // must contain at least one lowercase letter
                'regex:/[A-Z]/',      // must contain at least one uppercase letter
                'regex:/[0-9]/',      // must contain at least one digit
                'regex:/[!@#$%^&*]/', // must contain a special character
            ],
            'role' => [
                'string',
                'required',
            ]
        ];

        // If this is a newly created user, make certain fields mandatory.
        if (is_null($user)) {
            $rules['password'][] = 'required';
        } else {
            //Stop password being mandatory for existing users
            $rules['password'][] = 'nullable';
        }

        return $rules;
    }

    /**
     * Save the user to the database.
     *
     * @param  User  $user
     *
     * @return bool
     * @throws AuthorizationException
     */
    public function persist(User &$user)
    {
        // Save the user.
        $user->update($this->validated());

        //Updated Roles
        if ($this->has('role')) {
            $user->syncRoles($this->input('role'));
        }

        return $user;
    }

    public function validated()
    {
        $data = parent::validated();

        if (isset($data['password'])) {
            $data['password'] = Hash::make($data['password']);
        } else {
            unset($data['password']);
        }

        return $data;
    }
}